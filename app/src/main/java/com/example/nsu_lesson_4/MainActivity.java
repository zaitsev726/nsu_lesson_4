package com.example.nsu_lesson_4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements NewFragment.onMenuListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public void onAttach(String text) {
        getSupportFragmentManager().
                beginTransaction().
                add(R.id.constaraint, SecondFragment.newInstance(text)).
                commit();
    }
}